# vsn
 
VSN is a normalization tool dedicated to remove multiplicative and additive effects from spectra. 
matllab code.
some data files of the publication (doi://10.1002/cem.3164) are attached:

- mult_pur.mat          :   synthetic spectra with pure multiplicative effect
- mult_ldb_horiz.mat    :   synthetic spectra with multiplicative effect and constant baselines
- mult_lbd_slope.mat    :   synthetic spectra with multiplicative effect and slopes
- mult_lbd_parab.mat    :   synthetic spectra with multiplicative effect and parabolic baselines