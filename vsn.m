% previous action by BJ removed
function [z,res] = vsn(x, options)
% [z,res] = vsn(x, options);
%
% Carries out a VSN normalization. Same function is used to calculate and
% to apply the preprocessing
%
% input: the data (x) and a structure of parameters (options)
%
%  ** in calculation mode, options : a structure of parameters to be used, 
%     containing following fields. If options is omitted, vsn runs in
%     "full auto" mode 
%
% nparameters  : number of parameters of the effects model (Default : 2)
%   1, pure multiplicative effect 
%   2, multiplicative effect + horizontal baseline (SNV)
%   3, multplicative effect + quadratic baseline
%   ... and so on
%   'auto' : the value is computed as follows: 
%       - a grid search is built 
%       - weigths are calculated on this grid
%       - the highest std deviation of the calculated weights gives the tol value
%   'prompt' : as 'auto', but the user is prompted to enter the value on
%       the basis of std(w)
%
% tolerance   : tolerance for the ransac algorithm. (Default:'auto')
%   > 0 scalar : gives the tol parameter value (typ the level of spectral noise)
%   'auto' : the value is computed as follows: 
%       - a grid search is built 
%       - weigths are calculated on this grid
%       - the optimal tol value is retained
%   'prompt' : as 'auto', but the user is prompted to enter the value 
%
% ncouples  : number of couples of spectra used in the algorithm (default = 200 log10(N))
%
% niterations   : number of iteration used in ransac (default = 100 log10(P))
%
%  ** in apply mode, options is a structure containing :
% nparameters  : number of parameters of the effects model 
%   1, pure multiplicative effect 
%   2, multiplicative effect + horizontal baseline (SNV)
%   3, multplicative effect + quadratic baseline
%   ... and so on
%
% weights : a vector of weights to be applied 
%
%
% output :
% z     : the processed data
% res   : a structure with detailed results
%
%  examples of use :
% 
%               % full auto calculation on cal set
%       [Zc,res] = vsn(Xc); 
%               % application on test set
%       Zt = vsn(Xt, res);   
%
%               % user is prompted for tolerance value
%       [Zc,res] = vsn(Xc,struct('tolerance','prompt');
%
%               % suser is prompted for number of parameters
%       [Zc,res] = vsn(Xc,struct('nparameters','prompt');
%
%               % user is prompted for tolerance and nparameters values
%       [Zc,res] = vsn(Xc,struct('nparameters','prompt','tolerance','prompt');
%
%               % tolerance and nparameters values are given
%       [Zc,res] = vsn(Xc,struct('nparameters',3,'tolerance',0.003);
%

[n,p] = size(x);


% if only a matrix x is passed, mode 'full auto'
if nargin==1
    options = struct('tolerance','auto', 'nparameters','auto');
else
    % check for struct syntax
    f = fieldnames(options);
    auth = {'tolerance','nparameters','niterations','ncouples'};
    test = ismember(f,auth);
    if prod(test)==0
        error('one or more options are wrong. See help')
    end
end;

%%% managing default values
if isfield(options,'niterations')
    nit = options.niterations;
else
    nit = round(100*log10(p)); % empiric law ...
end;

if isfield(options,'ncouples')
    ncpl = options.ncouples;
else
    ncpl = round(200*log10(n)); % empiric law ...
end;

if isfield(options,'tolerance')
    tol = options.tolerance;
else
    tol = 'auto';   % automatic mode for tolerance
end;

if isfield(options,'nparameters')
    npar = options.nparameters;
else
    npar = 'auto';   % automatic mode for nparam
end;


% verification
if ischar(tol)
    if strcmp(lower(tol),'auto')==1
        tol = -1;
    elseif strcmp(lower(tol),'prompt')==1
        tol = -2;
    else
        error('invalid argument for tolerance');
    end;
end;
       
if ischar(npar)
    if strcmp(lower(npar),'auto')==1
        npar = -1;
    elseif strcmp(lower(npar),'prompt')==1
        npar = -2;
    else
        error('invalid argument for nparameters');
    end;
end;

if (tol <= 0) & tol ~=-1 & tol ~= -2
    error('tolerance value must be >=0' );
    return;
end;
if ((npar <= 0 & npar ~=-1 & npar ~= -2) || round(npar)~=npar)
    error('nparameters value must be an integer >0' );
    return;
end;

% init output parameters
z = x;
res = options;

if isfield(options, 'weights')  % apply mode
    w = options.weights;
    npar = options.nparameters;
    
else                            % calculation mode

    %%% weight calculation
    if (tol > 0) && (npar > 0)   % tolerance & npar values are given
        w = processNspectra( x, npar, tol, ncpl, nit );

    elseif tol<0 && npar<0 % full auto or prompt mode -> 2D grid search

        % building a grid search 

        s = std(x(:))/100;             % guess value for central point... to be checked
        gtol = s.*(10.^(-2:1:4));    % 10 points logarithmic grid around s
        ntol = length(gtol);
        gpar = 1:5;                    % till cubic baselines
        np = length(gpar);

        h = waitbar(0,'Please wait');
        for i=1:ntol                    % build the map of std(w)
            for j=1:np
                wg = processNspectra( x, gpar(j), gtol(i), ncpl, nit );
                st(i,j) = std(wg);
                w(i,j,:) = wg;
                waitbar(((i-1)*np+j)/(ntol*np));
            end;
        end;
        close(h);

        res.grid_search.grid_nparameters = gpar;
        res.grid_search.grig_tolerance = gtol;
        res.grid_search.std_weights = st;
        
        [sm,i] = max(st(:));        % find max location 
        ipr = floor((i-1)/ntol)+1;
        itl = mod(i-1,ntol)+1;

        if tol==-1  & npar==-1     % full automatic choice at the max
            npar = gpar(ipr);
            tol = gtol(itl);

            w = squeeze(w(itl,ipr,:))';

        elseif tol==-2  & npar==-2  % user is prompted for both parameters
            
            res.grid_search.grid_nparameters = gpar;
            res.grid_search.grig_tolerance = gtol;
            res.grid_search.std_weights = st;
            
            contourf(gpar,gtol,st);
            set(get(gcf,'CurrentAxes'),'yscale','log');
            ylabel('Tolerance');xlabel('Npar');
            grid on
            tol = input('Tolerance value: ');
            npar = input('Number of parameters: ');
            w = processNspectra( x, npar, tol, ncpl, nit );

        elseif tol==-2 & npar==-1   % user is prompted for tolerance value
            res.grid_search.grig_tolerance = gtol;
            res.grid_search.std_weights = st(:,ipr);
            
            npar = gpar(ipr);
            plot(gtol,st(:,ipr));
            set(get(gcf,'CurrentAxes'),'xscale','log');
            xlabel('Tolerance');ylabel('std(w)');
            grid on
            tol = input('Tolerance value: ');
            w = processNspectra( x, npar, tol, ncpl, nit );

        elseif tol==-1 & npar==-2   % user is prompted for nparameters value
            res.grid_search.grid_nparameters = gpar;
            res.grid_search.std_weights = st(itl,:);
            
            tol = gtol(itl);
            plot(gpar,st(itl,:));
            xlabel('N parameters');ylabel('std(w)');
            grid on
            npar = input('Number of parameters: ');
            w = processNspectra( x, npar, tol, ncpl, nit );
        end;
    
    elseif tol<0 && npar>0 % auto or prompt mode for tolerance -> 1D grid search
        
        % building a grid search 

        s = std(x(:))/100;           % guess value for central point... to be checked
        gtol = s.*(10.^(-2:1:4));    % 10 points logarithmic grid around s
        ntol = length(gtol);
        res.grid_search.grig_tolerance = gtol;
        
        h = waitbar(0,'Please wait');
        for i=1:ntol                    % build the curve of std(w)
            wg = processNspectra( x, npar, gtol(i), ncpl, nit );
            st(i) = std(wg);
            w(i,:) = wg;
            waitbar(i/ntol);
        end;
        close(h);
        res.grid_search.std_weights = st;
        
        if tol == -1    % automatic mode for tol
            [sm,i] = max(st);
            w = w(i,:);
            tol = gtol(i);
        else            % user is prompted for tolerance value
            plot(gtol,st);
            set(get(gcf,'CurrentAxes'),'xscale','log');
            xlabel('Tolerance');ylabel('std(w)');
            grid on
            tol = input('Tolerance value: ');
            w = processNspectra( x, npar, tol, ncpl, nit );
        end;
        
    elseif tol>0 && npar<0 % auto or prompt mode for nparameters -> 1D grid search
        % building a grid search 

        gpar = 1:5;                    % till cubic baselines
        np = length(gpar);
        res.grid_search.grid_nparameters = gpar;
        
        h = waitbar(0,'Please wait');
        for i=1:np                    % build the curve of std(w)
            wg = processNspectra( x, gpar(i), tol, ncpl, nit );
            st(i) = std(wg);
            w(i,:) = wg;
            waitbar(i/np);
        end;
        close(h);
        res.grid_search.std_weights = st;
        
        if npar == -1    % automatic mode for nparameters
            [sm,i] = max(st);
            w = w(i,:);
            npar = gpar(i);
        else            % user is prompted for nparameters
            plot(gpar,st);
            xlabel('N parameters');ylabel('std(w)');
            grid on
            npar = input('Number of parameters: ');
            w = processNspectra( x, npar, tol, ncpl, nit );
        end;
        
    end;
    
    w = w.^2;      % weigths are squared in order to increase the contrast

    % record results
    res.nparameters = npar;
    res.tolerance = tol;
    res.weights = w;
end;

% applying the weigthed preprocessing
z = ApplyWeightedCorrection(x, w, npar);

end


%%% weight building fuctions

% main function, run on the whole matrix
function w = processNspectra( x, k, tol, ncpl, nit )
% w = processNspectra( x, k, tol, ncpl, nit )
%
% runs the computation for ncpl couples of spectra, using nit iterations 
%    and a tolerance tol for each
%
% x : spectra matrix to be processed
% k : number of parameters of the 
%   1, pure multiplicative effect 
%   2, multplicative effect + horizontal baseline 
%   3, multplicative effect + quadratic baseline
%   ... and so on
% tol : the tolerance used in RANSAC algorithm
% ncpl : number of couples to test
% nit : number of iterations used in Ransac

[n,p] = size(x);

ind = zeros(ncpl,p);

for i=1:ncpl
    id = randperm(n,2);
    % compute the inliers
    index = SpectraCoupleWLSelection( x(id(1),:), x(id(2),:), k, tol, nit);
    % memorizes thz inliers 
    ind(i,index) = 1;
end;

% compute the weights as the probability a wavelength be an inlier
w = sum(ind)./ncpl;

end % Function ProcessNSpectra

% local function, run on each couple of spectra
function IndexWL = SpectraCoupleWLSelection( S1, S2, k, tolerance, iterations)
% L = SpectraCoupleWLSelection( S1, S2, k, tol, iter)
%
% determine the best selection of wavelengths L for which one can write: 
% S2 = coeff * [S1 1 L L^2 ... L^(k-2)] + epsilon 
% where  abs(epsilon) is lower than a tolerance threshold

%Inputs:
% S1, S2: spectra  as line vectors
% k : number of parameters of the model
%   1 : simple mulptiplicative effect
%   2 : multiplicative effect + horizontal baseline
%   3 : multiplicative effect + slope baseline
%   ...
% tol: tolerance threshold for epsilon
%   Return values:
%
% L:  indexes of selected wavelengths

p = size(S1,2);
if(p ~= size(S2,2))
    error(...
    sprintf('S1(%d) and S2(%d) have different sizes ! ',p, size(S2,2)));
end
k = round(k);
if k<1
    error('k parameter should be >= 1');
end;

% preparing Vandermond matrix (polynoms row-wise)
L = ones(k-1,p);
for i=1:k-2
    L(i+1,:) = (1:p).^i;
end

% initialization
lm = 0;
IndexWL = 1:p;

% loop
for i=1:iterations
    % drawing k variables
    il = randperm(p,k);
    % building the system for resolution
    x1 = S1(:,il)';
    x2 = [S2(:,il);L(:,il)]';
    if rank(x2)==size(x2,1)
        % solving the system
        b = x2\x1;
        % applying the model and calculating the difference
        if isempty(find(isnan(b),1))
            idx = find(abs(b'*[S2;L] - S1) <= tolerance);
            l = length(idx);
            if lm < l
                lm = l;
                IndexWL = idx;
            end
        end
    end
end

end % function SpectraCoupleWL Selection


%%% weight using fuctions

function z = ApplyWeightedCorrection(x, w, npar) 

[n,p] = size(x);

sw = sum(w); % will be used for re-normalization

% make a proba
w0 = w;
w = w ./ sw;

% create weighting matrix
W = diag(w);

if npar>1
    %removing additive effect
    L = ones(p,npar-1); % preparing Vandermond matrix
    for i=1:npar-2
        L(:,i+1) = (1:p)'.^i;
    end

    % weighted detrend 
    z = x - x * W * L * pinv(L'*W*L) * L';
else
    z = x;
end

%normalization   
s = std((z*W)')';  %% version of the paper
z = z ./ repmat(s,1,p);
z = z ./ sw;

end




